Given /^I am on the Welcome Screen$/ do
  element_exists("view")
  sleep(STEP_PAUSE)
end

Given(/^i'm on landing page$/) do
	 sleep 2
end

When(/^I enter the username and password$/) do
	wait_for_elements_exist ["* id:'icon-username-field'"], timeout:20
	touch("* id:'icon-username-field'")
	keyboard_enter_text 'testing application'
	touch("* id:'icon-password-field'")
	keyboard_enter_text 'testing application'
end

Then(/^I sign in to the application$/) do
	query("* marked:'Sign In'")

end